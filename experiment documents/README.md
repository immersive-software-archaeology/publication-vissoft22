### `experiment documents/`

This folder contains **blanko versions** of our experiment's *entry survey* and *task session protocol*.

> ##### [`Entry Survey Blanko.pdf`](Task%20Sheet%20Blanko.pdf?raw=true)
>
> A PDF version of the entry survey questions used to capture participant's prior knowledge and experience with aspects relevant to our experiment.
> Generated via Google Forms' PDF export feature.

> ##### [`Task Sheet Blanko.pdf`](Task%20Sheet%20Blanko.pdf?raw=true)
>
> A blanko PDF version of the tasks given during the experiment's tool session along with meta instructions and aspects to consider for the experiment host.
