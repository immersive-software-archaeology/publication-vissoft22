### `results/`

This folder contains **raw and more detailed data** for the experiment presented in the publication.

 > ##### [`Results BarPlots.pdf`](Results%20BarPlots.pdf?raw=true)
 >
 > A PDF with further results from the experiment (that were not reported on in the paper due to space constraints).
 > The shown results are grouped by used tool.

 > ##### [`Results Entry Survey Unsorted.pdf`](Results%20Entry%20Survey%20Unsorted.pdf?raw=true)
 >
 > A PDF with unsorted (in terms of participants' tool group membership) results of the entry survey.
 > These were fetched from Google Forms via an export and are not further processed.

 > ##### [`Results Raw.csv`](Results%20Raw.csv?raw=true)
 >
 > Raw data from the experiment's tool session in CSV machine-readable format.

 > ##### [`Results Raw.pdf`](Results%20Raw.pdf?raw=true)
 >
 > Raw data from the experiment's tool session in PDF format (for better legibility).

 **Please note**: Due to privacy concerns, we do not include any of our video and audio recordings.
