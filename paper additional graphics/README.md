### `paper additional graphics/`

This folder contains **additional figures** for our publication.

> ##### [`software-structure-metamodel.pdf`](software-structure-metamodel.pdf?raw=true)
>
> Metamodel for the design and architecture of object-oriented software systems as conceptually used by our technique.
