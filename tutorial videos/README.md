### `tutorial videos/`

This folder contains the **training videos** for the experiment presented in the publication.

> ##### [`explanation_argouml.mp4`](explanation_argouml.mp4?raw=true)
>
> Explanation video for the subject system ArgoUML.
> The video was shown during the entry survey.

> ##### [`explanation_eclipse.mp4`](explanation_eclipse.mp4?raw=true) [`explanation_cityvr.mp4`](explanation_cityvr.mp4?raw=true) [`explanation_isa.mp4`](explanation_isa.mp4?raw=true)
>
> Explanation video for the three different tools used in our experiment.
> One of the videos was shown at the end of the entry survey (depending on which tool a participant was randomly assigned to).
