# Publication VISSOFT22 - Complementing Material

This repository serves as a consistent container for supplementing material to our VISSOFT'22 publication "Utilizing Software Architecture Recovery to Explore Large-Scale Software Systems in Virtual Reality".

Link to the paper: [**preprint_vissoft22.pdf**](https://pure.itu.dk/ws/portalfiles/portal/92854114/preprint_vissoft22.pdf)


&nbsp;


## **Repository Organization**

> ### [`experiment documents/`](experiment%20documents/)
>
> This folder contains **blanko versions** of our experiment's *entry survey* and *task session protocol.*  
> [Further information](experiment%20documents/).
>
>   - [`Entry Survey Blanko.pdf`](experiment%20documents/Task%20Sheet%20Blanko.pdf?raw=true)
>   - [`Task Sheet Blanko.pdf`](experiment%20documents/Task%20Sheet%20Blanko.pdf?raw=true)

---

> ### [`paper additional graphics/`](paper%20additional%20graphics/)
>
> This folder contains **additional figures** for our publication (see above).  
> [Further information](paper%20additional%20graphics/).
>
>   - [`software-structure-metamodel.pdf`](paper%20additional%20graphics/software-structure-metamodel.pdf?raw=true)

---

> ### [`results/`](results/)
>
> This folder contains **raw and more detailed data** for the experiment presented in the publication.  
> [Further information](results/).
>
>   - [`Results BarPlots.pdf`](results/Results%20BarPlots.pdf?raw=true)
>   - [`Results Entry Survey Unsorted.pdf`](results/Results%20Entry%20Survey%20Unsorted.pdf?raw=true)
>   - [`Results Raw.csv`](results/Results%20Raw.csv?raw=true)
>   - [`Results Raw.pdf`](results/Results%20Raw.pdf?raw=true)

---

> ### [`tutorial videos/`](tutorial%20videos/)
>
> This folder contains the **training videos** for the experiment presented in the publication.  
> [Further information](tutorial%20videos/).
>
>   - [`explanation_argouml.mp4`](tutorial%20videos/explanation_argouml.mp4?raw=true)
>   - [`explanation_cityvr.mp4`](tutorial%20videos/explanation_cityvr.mp4?raw=true)
>   - [`explanation_eclipse.mp4`](tutorial%20videos/explanation_eclipse.mp4?raw=true)
>   - [`explanation_isa.mp4`](tutorial%20videos/explanation_isa.mp4?raw=true)

---


&nbsp;


## **Tool Implementation: Eclipse**

The Eclipse IDE can be downloaded via the [download section](https://www.eclipse.org/downloads/packages/) on its homepage.
We recommend installing the *"Eclipse IDE for Java Developers"*.  
Eclipse Version used in the experiment:
[Eclipse 2021-09](https://www.eclipse.org/downloads/packages/release/2021-09/r)


## **Tool Implementation: CityVR**

[CityVR](https://scg.unibe.ch/research/cityvr) can be installed by checking out its [repository on BitBucket](https://bitbucket.org/leonelmerino/cityvr/src/master/) and opening it as Unity project.
We recommend using the [Unity Hub](https://unity3d.com/get-unity/download), as it eases finding and installing the correct Unity version for the project.  
Using CityVR requires a VR headset that is supported by SteamVR.
CityVR was developed for the HTC Vive, other headsets should be compatible.


## **Tool Implementation: Immersive Software Archaeology**

The tool implementation of ISA is split in two repositories.
These contain elaborate explanations on installation and usage respectively.  
Using ISA requires a VR headset that is supported by SteamVR.
ISA was developed for the Valve Index, other headsets should be compatible.

#### [Immersive Software Archaeology Eclipse Plugins](https://gitlab.com/immersive-software-archaeology/immersive-software-archaeology-eclipse)

- An Eclipse runtime along with the ISA Eclipse plugins provides functionality for...
    1. analyzing subject software systems and
    2. providing the ISA Virtual Reality Application with information on previously analyzed systems.

#### [Immersive Software Archaeology Virtual Reality Application](https://gitlab.com/immersive-software-archaeology/immersive-software-archaeology-vr)

- The ISA VR application displays software systems in virtual reality.
- It requires the ISA Eclipse plugins to run in the backgorund.


&nbsp;



## **Contact**

In case you have questions, please don't hesitate to contact me: [adho@itu.dk](adho@itu.dk).



&nbsp;
